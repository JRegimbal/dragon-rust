extern crate svg;

use std::env;
use std::collections::VecDeque;
use std::iter::FromIterator;
use svg::Document;
use svg::node::element::{Path};
use svg::node::element::path::{Data,Position,Command,Parameters};

fn main() {
    let args: Vec<String> = env::args().collect();
    let first_arg = match args.get(1) {
        Some(x) => x,
        _       => "10",
    };
    let iterations: usize = match first_arg.parse() {
        Ok(x)   => x,
        _       => { println!("Could not parse {}", first_arg); 10 },
    };

    let data = gen_data(dragon(iterations));
    let path = Path::new()
        .set("fill", "none")
        .set("stroke", "black")
        .set("stroke-width", 1)
        .set("d", data);
    let document = Document::new()
        .set("viewBox", (0, 0, 1000, 1000))
        .add(path);
    svg::save("image.svg", &document).unwrap();
}

struct Memo {
    memoized: Vec<Vec<bool>>,
}

impl Memo {
    fn new() -> Memo {
        Memo { memoized: vec![vec![true]] }
    }

    fn at(&mut self, n: usize) -> Vec<bool> {
        if n < self.memoized.len() {
            return self.memoized[n].clone();
        }
        let mut val: Vec<bool> = self.at(n-1);
        val.push(true);
        val.append(&mut self.not_memoized(n-1));
        self.memoized.push(val);
        self.memoized[n].clone()
    }

    fn not_memoized(&self, n: usize) -> Vec<bool> {
        let mut rev: VecDeque<bool> = VecDeque::new();
        for elem in &self.memoized[n] {
            rev.push_front(!elem);
        }
        Vec::from_iter(rev.into_iter())
    }
}

fn dragon(n: usize) -> Vec<bool> {
    if n <= 0 {
        return vec![];
    }
    else if n == 1 {
        return vec![true];
    }
    let mut memo = Memo::new();
    memo.at(n)
}

fn gen_data(curve: Vec<bool>) -> Data {
    let mut commands: Vec<Command> = vec![Command::Move(Position::Absolute, Parameters::from((500, 500))), Command::Line(Position::Relative, Parameters::from((0, 1)))];
    let mut dir: i8 = 0;
    for elem in curve {
        if elem {
            dir = (dir + 5) % 4;
        } else {
            dir = (dir + 3) % 4;
        }

        match dir {
            0   => { commands.push(Command::Line(Position::Relative, Parameters::from((0, 1)))); },
            1   => { commands.push(Command::Line(Position::Relative, Parameters::from((1, 0)))); },
            2   => { commands.push(Command::Line(Position::Relative, Parameters::from((0, -1)))); },
            3   => { commands.push(Command::Line(Position::Relative, Parameters::from((-1, 0)))); },
            _   => { println!("An error has occured: {}", dir); }
        }
    }
    Data::from(commands)
}
